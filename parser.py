#!/usr/bin/env python3

# Sopeq: parser.py
# License: GPLv2
# Author: Brad Cable

from re import compile

from incoming import incoming
from outgoing import outgoing
from pool import pool
from service import service
from traffic import traffic

class line_parser(object):
	_controller = None
	_line = None
	_skip = None
	_first_if = None
	_first_addr = None
	_second_if = None
	_second_addr = None

	def __init__(self, controller, line):
		self._controller = controller
		self._line = line
		self._skip = False

	def parse(self, act=True):
		line = self._line.decode().strip()
		if len(line) == 0 or line[0] == "#":
			self._skip = True
			return None

		first_split = line.split(" ", maxsplit=1)

		if len(first_split) != 2:
			return self.error(act)

		preamble, the_rest = first_split
		second_split = preamble.split(":")

		if len(second_split) != 2:
			return self.error(act)

		class_action, modifier = second_split
		third_split = class_action.split(".")

		if len(third_split) != 2:
			return self.error(act)

		obj_class, action = third_split

		if obj_class not in (
			"custom", "incoming", "outgoing", "pool", "service", "traffic"
		):
			return self.error(act)

		# grab associated method in this class for the rule
		method = self.__getattribute__(obj_class)

		# exceptions to regexp parsing
		if obj_class in ("custom", "pool"):
			# call discovered method and return
			return method(action, modifier, the_rest, act=act)
		else:
			# parses the tail end of the rule with regexps and takes them out
			# of the_rest into variables as follows:
			#  body [log_prefix] # rule_comment
			re_end = compile("((\[([^\]]*)\])?[\t ]*(\#[\t ]*(.*))?)?$")
			re_end = re_end.search(the_rest)
			log_prefix = re_end.group(3)
			rule_comment = re_end.group(5)
			body = the_rest[0:re_end.start()].strip()

			# call discovered method and return
			return method(
				action, modifier, body, log_prefix, rule_comment, act=act
			)

	def error(self, act=True):
		if act:
			raise Exception # TODO
		else:
			return False

	def instance_parse(self, body, act=True):
		main_split = body.split(" ")
		if len(main_split) != 2:
			return self.error(act)

		first_arg, second_arg = main_split

		first_split = first_arg.split(",")
		if len(first_split) > 2:
			return self.error(act)
		elif len(first_split) == 2:
			self._first_if = first_split[0]
			self._first_addr = first_split[1]
		else:
			self._first_if = None
			self._first_addr = first_split[0]

		second_split = second_arg.split(",")
		if len(second_split) > 2:
			return self.error(act)
		elif len(second_split) == 2:
			self._second_if = second_split[0]
			self._second_addr = second_split[1]
		else:
			self._second_if = None
			self._second_addr = second_split[0]

		return True

	def instance_call(self, obj_call, obj_name,
		target, service_name, body, log_prefix, rule_comment, act=True
	):
		if target not in ("accept", "drop", "reject"):
			return self.error(act)

		self.instance_parse(body, act=act)

		if act:
			return obj_call(self._controller, target, service_name,
				log_prefix, rule_comment,
				self._first_addr, self._second_addr,
				self._first_if, self._second_if
			)
		else:
			return "{}({}, {}, {}, {}, {}, {})".format(
				obj_name, target, service_name,
				self._first_addr, self._second_addr,
				self._first_if, self._second_if
			)

	def incoming(self,
		target, service_name, body, log_prefix, rule_comment, act=True
	):
		return self.instance_call(incoming, "incoming",
			target, service_name, body, log_prefix, rule_comment, act
		)

	def outgoing(self,
		target, service_name, body, log_prefix, rule_comment, act=True
	):
		return self.instance_call(outgoing, "outgoing",
			target, service_name, body, log_prefix, rule_comment, act
		)

	def traffic(self,
		target, service_name, body, log_prefix, rule_comment, act=True
	):
		return self.instance_call(traffic, "traffic",
			target, service_name, body, log_prefix, rule_comment, act
		)

	def service(self,
		action, service_name, rule, log_prefix, rule_comment, act=True
	):
		if action not in ("in", "out"):
			return self.error(act)

		if act:
			return service(self._controller,
				action, service_name, rule, log_prefix, rule_comment
			)
		else:
			return "service({}, {}, {})".format(action, service_name, rule)

	def pool(self, action, pool_name, the_rest, act=True):
		if action not in ("new", "add", "join"):
			return self.error(act)

		the_rest_list = the_rest.split(" ") # the young and the_rest_list
		working_pool = self._controller.get_pool(pool_name)

		if action == "add" or action == "new":
			if working_pool is None:
				if action == "add":
					raise Exception # TODO: pool not defined

				working_pool = pool(pool_name)

			if act:
				for ip_addr in the_rest_list:
					working_pool.add(ip_addr)
			else:
				pass # TODO: asserts verification stuff

		elif action == "join":
			if act:
				for join_pool_name in the_rest_list:
					join_pool = self._controller.get_pool(join_pool_name)

					if join_pool is None:
						raise Exception # TODO: pool not found
					else:
						working_pool.join(join_pool)

			else:
				pass # TODO: asserts verification stuff

		return working_pool # TODO: asserts want something else returned

if __name__ == "__main__":
	assert line_parser(
		b"     # test comment  "
	).parse(act=False) is None
	assert line_parser(
		b"  						"
	).parse(act=False) is None
	assert line_parser(
		b"  whatever "
	).parse(act=False) is False

	assert line_parser(
		b"service.in:ssh -p tcp --dport 22"
	).parse(act=False) == "service(in, ssh, -p tcp --dport 22)"
	assert line_parser(
		b"service.out:ssh -p tcp --dport 22"
	).parse(act=False) == "service(out, ssh, -p tcp --dport 22)"

	assert line_parser(
		b"incoming.accept:ssh eth0,10.1.1.1/32 eth1,10.0.0.0/8"
	).parse(act=False) == \
		"incoming(accept, ssh, 10.1.1.1/32, 10.0.0.0/8, eth0, eth1)"
	assert line_parser(
		b"incoming.drop:ssh eth0,10.1.1.1/32 eth1,10.0.0.0/8"
	).parse(act=False) == \
		"incoming(drop, ssh, 10.1.1.1/32, 10.0.0.0/8, eth0, eth1)"
	assert line_parser(
		b"incoming.reject:ssh eth0,10.1.1.1/32 eth1,10.0.0.0/8"
	).parse(act=False) == \
		"incoming(reject, ssh, 10.1.1.1/32, 10.0.0.0/8, eth0, eth1)"

	assert line_parser(
		b"outgoing.accept:ssh eth0,10.1.1.1/32 eth1,10.0.0.0/8"
	).parse(act=False) == \
		"outgoing(accept, ssh, 10.1.1.1/32, 10.0.0.0/8, eth0, eth1)"
	assert line_parser(
		b"outgoing.drop:ssh eth0,10.1.1.1/32 eth1,10.0.0.0/8"
	).parse(act=False) == \
		"outgoing(drop, ssh, 10.1.1.1/32, 10.0.0.0/8, eth0, eth1)"
	assert line_parser(
		b"outgoing.reject:ssh eth0,10.1.1.1/32 eth1,10.0.0.0/8"
	).parse(act=False) == \
		"outgoing(reject, ssh, 10.1.1.1/32, 10.0.0.0/8, eth0, eth1)"

	assert line_parser(
		b"traffic.accept:ssh eth0,10.1.1.1/32 eth1,10.0.0.0/8"
	).parse(act=False) == \
		"traffic(accept, ssh, 10.1.1.1/32, 10.0.0.0/8, eth0, eth1)"
	assert line_parser(
		b"traffic.drop:ssh eth0,10.1.1.1/32 eth1,10.0.0.0/8"
	).parse(act=False) == \
		"traffic(drop, ssh, 10.1.1.1/32, 10.0.0.0/8, eth0, eth1)"
	assert line_parser(
		b"traffic.reject:ssh eth0,10.1.1.1/32 eth1,10.0.0.0/8"
	).parse(act=False) == \
		"traffic(reject, ssh, 10.1.1.1/32, 10.0.0.0/8, eth0, eth1)"

	assert line_parser(
		b"incoming.accept:ssh 10.1.1.1/32 eth1,10.0.0.0/8"
	).parse(act=False) == \
		"incoming(accept, ssh, 10.1.1.1/32, 10.0.0.0/8, None, eth1)"
	assert line_parser(
		b"incoming.accept:ssh eth0,10.1.1.1/32 10.0.0.0/8"
	).parse(act=False) == \
		"incoming(accept, ssh, 10.1.1.1/32, 10.0.0.0/8, eth0, None)"
	assert line_parser(
		b"incoming.accept:ssh 10.1.1.1/32 10.0.0.0/8"
	).parse(act=False) == \
		"incoming(accept, ssh, 10.1.1.1/32, 10.0.0.0/8, None, None)"

	print("All asserts passed.")
