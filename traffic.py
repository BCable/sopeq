#!/usr/bin/env python3

# Sopeq: traffic.py
# License: GPLv2
# Author: Brad Cable

from service_instance import service_instance

class traffic(service_instance):

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

if __name__ == "__main__":
	assert traffic(
		"accept", "ssh",
		"10.1.1.1/32", "10.0.0.0/8",
		"eth0", "eth1"
	).rules() == [
	("filter",
		"-A ssh_IN -d 10.1.1.1/32 -s 10.0.0.0/8 -i eth0 -o eth1 -j ACCEPT",
	),("filter",
		"-A ssh_OUT -s 10.1.1.1/32 -d 10.0.0.0/8 -o eth0 -i eth1 -j ACCEPT"
	)]

	assert traffic(
		"accept", "ssh",
		None, "10.0.0.0/8",
		"eth0", "eth1"
	).rules() == [
	("filter",
		"-A ssh_IN -s 10.0.0.0/8 -i eth0 -o eth1 -j ACCEPT",
	),("filter",
		"-A ssh_OUT -d 10.0.0.0/8 -o eth0 -i eth1 -j ACCEPT"
	)]

	assert traffic(
		"accept", "ssh",
		"10.1.1.1/32", None,
		"eth0", "eth1"
	).rules() == [
	("filter",
		"-A ssh_IN -d 10.1.1.1/32 -i eth0 -o eth1 -j ACCEPT",
	),("filter",
		"-A ssh_OUT -s 10.1.1.1/32 -o eth0 -i eth1 -j ACCEPT"
	)]

	assert traffic(
		"accept", "ssh",
		"10.1.1.1/32", "10.0.0.0/8",
		None, "eth1"
	).rules() == [
	("filter",
		"-A ssh_IN -d 10.1.1.1/32 -s 10.0.0.0/8 -o eth1 -j ACCEPT",
	),("filter",
		"-A ssh_OUT -s 10.1.1.1/32 -d 10.0.0.0/8 -i eth1 -j ACCEPT"
	)]

	assert traffic(
		"accept", "ssh",
		"10.1.1.1/32", "10.0.0.0/8",
		"eth0", None
	).rules() == [
	("filter",
		"-A ssh_IN -d 10.1.1.1/32 -s 10.0.0.0/8 -i eth0 -j ACCEPT",
	),("filter",
		"-A ssh_OUT -s 10.1.1.1/32 -d 10.0.0.0/8 -o eth0 -j ACCEPT"
	)]

	print("All asserts passed.")
