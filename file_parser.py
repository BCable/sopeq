#!/urs/bin/env python

import re, shlex

from class_loader import ClassLoader

class FileLine(object):
	line = None
	comment = None
	cls = None
	method = None
	method_args = None
	command_args = None
	parsed = False

	def __init__(self, line=None):
		if line is not None:
			self.line = line
			self.parse()

	def __str__(self):
		string = "FileLine(\"{}\")\n".format(self.line)
		string += " - comment: {}\n".format(self.comment)
		string += " - class: {}\n".format(self.cls)
		string += " - method: {}\n".format(self.method)
		string += " - method arguments: {}\n".format(self.method_args)
		string += " - command arguments: {}\n".format(self.command_args)
		return string

	def parse(self, line=None):
		if line is None:
			if self.line is None:
				raise TypeError
			else:
				line = self.line
		else:
			self.line = line

		re_comment = "^([^\#]+)[ \t]*\#[ \t]*(.*)$"
		if re.match(re_comment, line) is None:
			parseable = line
			self.comment = None
		else:
			parseable = re.sub(re_comment, '\\1', line)
			self.comment = re.sub(re_comment, '\\2', line)

		line_arr = shlex.split(parseable)
		first_arg = line_arr.pop(0)
		self.command_args = line_arr

		self.first_arg(first_arg)

	def first_arg(self, arg):
		method_args = arg.split(":")
		cls_split = method_args.pop(0).split(".")
		self.cls = cls_split[0]

		if len(cls_split) > 1:
			self.method = cls_split[1]
		else:
			self.method = "default"

		self.method_args = method_args


class FileParser(object):
	file_lines = []

	def __init__(self, filename=None):
		if filename is not None:
			self.load(filename)

	def __str__(self):
		pass

	def get_lines(self):
		return self.file_lines

	def load(self, filename):
		f = open(filename, "r")
		fl_lines = []
		for line in f:
			fl_lines.append(FileLine(line))

		success = True
		for fl in fl_lines:
			cl = ClassLoader()
			if cl.load_class(fl.cls):
				if not cl.load_method(fl.method):
					print("TODO: ERROR")
					success = False
					break
			else:
				print("TODO: ERROR")
				success = False
				break

			cl.load_method_args(fl.method_args)
			cl.load_command_args(fl.command_args)

			self.file_lines.append(cl)

		return success

	def execute(self):
		for line in self.file_lines:
			line.execute()



if __name__ == "__main__":
	print(FileLine("pool:serverpool1 192.168.0.0/16"))
	print(FileLine("pool:serverpool2 10.0.0.0/8"))
	print(FileLine("pool.join:serverpool serverpool1 serverpool2"))
	print(FileLine("pool:clientpool 192.168.0.1/32"))
	print(FileLine("service:http:forin -p tcp --dport 80"))
	print(FileLine("service:http:forout -p tcp --sport 80 # http forwarding output"))
	print(FileLine("service.pool:http clientpool serverpool"))
	print(FileLine("service.pool:http:eth0 clientpool serverpool"))
	print(FileLine("pool.clear:serverpool"))
	print(FileLine("pool.clear:clientpool # klasjdkalsjdl"))

	fp = FileParser("rules_testfile.conf")
	fp.execute()
