#!/usr/bin/env python3

# Sopeq: iptables_helpers.py
# License: GPLv2
# Author: Brad Cable

def ipt_escape(string):
	return string.replace("\\", "\\\\").replace("\"", "\\\"")

def ipt_construct_log(log_level, log_prefix=None):
	#log_ret = " -j LOG --log-tcp-options --log-ip-options --log-uid "
	log_ret = " -j LOG "

	if log_prefix is not None:
		log_ret += "--log-level {} --log-prefix \"{}: \"".format(
			log_level, ipt_escape(log_prefix)
		)
	else:
		log_ret += "--log-level {}".format(log_level)

	return log_ret

def ipt_construct_comment(rule_comment):
	return " -m comment --comment \"{}\"".format(ipt_escape(rule_comment))

def ipt_clean(iptables_output):
	clean = sub(b"^#[^\n]+\n", b"", iptables_output)
	clean = sub(b"\n#[^\n]+(?=\n)", b"\n", clean)
	clean = sub(b" \[[0-9]+:[0-9]+\]\n", b"\n", clean)
	return clean
