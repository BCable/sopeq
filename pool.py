#!/usr/bin/env python3

# Sopeq: pool.py
# License: GPLv2
# Author: Brad Cable

class pool(object):
	name = None
	_ip_addrs = None

	def __init__(self, name):
		self.name = name
		self._ip_addrs = []

	def add(self, ip_addr):
		self._ip_addrs.append(ip_addr)

	def join(self, pool):
		self._ip_addrs.extend(pool.get_all_ips())

	def get_all_ips(self):
		return self._ip_addrs

if __name__ == "__main__":
	intranet = pool("intranet")
	intranet.add("10.0.0.0/8")
	intranet.add("172.16.0.0/12")
	intranet.add("192.168.0.0/16")
	intranet.add("224.0.0.0/4")
	assert intranet.get_all_ips() == [
		"10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16", "224.0.0.0/4"
	]

	pool1 = pool("pool1")
	pool1.add("192.168.0.1")
	pool2 = pool("pool2")
	pool2.add("192.168.0.2")
	pool2.join(pool1)
	assert pool2.get_all_ips() == ["192.168.0.2", "192.168.0.1"]

	print("All asserts passed.")
