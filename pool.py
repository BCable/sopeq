#!/usr/bin/env python

import iptables

pools = {}

class default(object):
	name = None

	def __init__(self, name):
		self.name = name

	def call(self, *command_args):
		if self.name in pools:
			pools[self.name].extend(command_args)
		else:
			pools[self.name] = list(command_args)

class join(object):
	name = None

	def __init__(self, name):
		self.name = name

	def call(self, *join_pools):
		if self.name not in pools:
			pools[self.name] = []

		for join_pool in join_pools:
			pools[self.name].extend(pools[join_pool])

class clear(object):

	def __init__(self):
		pass
