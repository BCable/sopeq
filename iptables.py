#!/usr/bin/env python

IPTABLES_PATH="/usr/sbin/iptables"

import subprocess

def run(command_args, wait=True):
	whole_command = [IPTABLES_PATH].extend(command_args)

	proc = subprocess.Popen(
		whole_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE
	)

	if wait:
		proc.wait()
