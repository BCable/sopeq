#!/usr/bin/env python3

# Sopeq: service.py
# License: GPLv2
# Author: Brad Cable

from iptables_helpers import *
from rule_generator import rule_generator

class service(rule_generator):
	_controller = None
	direction = None
	name = None
	_rule = None
	_log_prefix = None
	_rule_comment = None
	_forwarding = None
	_chains = None
	_rules = None
	_cache_construct_comment = None

	def __init__(self,
		controller, direction, name, rule, log_prefix, rule_comment
	):
		self._controller = controller
		self.direction = direction
		self.service_name = name
		self.name = "{}:{}".format(name, direction)
		self._rule = rule
		self._log_prefix = log_prefix
		self._rule_comment = rule_comment

	def chains(self):
		return self._chains

	def rules(self):
		return self._rules

	def append_rule(self, rule):
		if self._cache_construct_comment is not None:
			if self._rule_comment is not None:
				self._cache_construct_comment = \
					ipt_construct_comment(self._rule_comment)
			else:
				self._cache_construct_comment = ""

			rule += self._cache_construct_comment

		self._rules.append(("filter", rule))

	def generate(self, *args, **kwargs):
		super().generate(*args, **kwargs)

		self._rules = []
		self._chains = []

		if self.direction == "in":
			new_chain = "{}_IN".format(self.service_name)
			direction_table = "INPUT"

		elif self.direction == "out":
			if not self._egress:
				return

			new_chain = "{}_OUT".format(self.service_name)
			direction_table = "OUTPUT"

		else:
			raise Exception # TODO

		self._chains.append(("filter", new_chain))

		if self._log_prefix is not None:
			log_append = ipt_construct_log(
				self._controller.log_level, self._log_prefix
			)

			cur_rule = "-A {} {}{}".format(
				direction_table, self._rule, log_append
			)

			if comment_append is not None:
				cur_rule += comment_append

			rules.append(("filter", cur_rule))

		self.append_rule("-A {} {} -j {}".format(
			direction_table, self._rule, new_chain
		))

		if self._forwarding:
			if self._log_prefix is not None:
				self.append_rule("-A FORWARD {}".format(log_append))

			self.append_rule(
				"-A FORWARD {} -j {}".format(self._rule, new_chain)
			)

if __name__ == "__main__":
	ssh = service("in", "ssh", "-p tcp --dport 22")
	ssh.generate()
	assert ssh.chains() == [("filter", "ssh_IN")]
	assert ssh.rules() == [
		("filter", "-A INPUT -p tcp --dport 22 -j ssh_IN")
	]

	ssh = service("out", "ssh", "-p tcp --sport 22")
	ssh.generate()
	assert ssh.chains() == [("filter", "ssh_OUT")]
	assert ssh.rules() == [
		("filter", "-A OUTPUT -p tcp --sport 22 -j ssh_OUT")
	]

	ssh = service("in", "ssh", "-p tcp --dport 22")
	ssh.generate(forwarding=True)
	assert ssh.chains() == [("filter", "ssh_IN")]
	assert ssh.rules() == [
		("filter", "-A INPUT -p tcp --dport 22 -j ssh_IN"),
		("filter", "-A FORWARD -p tcp --dport 22 -j ssh_IN")
	]

	ssh = service("out", "ssh", "-p tcp --sport 22")
	ssh.generate(forwarding=True)
	assert ssh.chains() == [("filter", "ssh_OUT")]
	assert ssh.rules() == [
		("filter", "-A OUTPUT -p tcp --sport 22 -j ssh_OUT"),
		("filter", "-A FORWARD -p tcp --sport 22 -j ssh_OUT")
	]

	print("All asserts passed.")
