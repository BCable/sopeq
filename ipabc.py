#!/usr/bin/env python

# Sopeq: ipabc.py
# License: GPLv2
# Author: Brad Cable

import abc

# IP address abstract class
# implementation examples would include IPv4 and IPv6

class IPABC(object):
	_addr_input = None

	def __init__(self, addr):
		super().__init__()
		self._addr_input = addr

	@abc.abstractmethod
	def valid(self, addr):
		return None
