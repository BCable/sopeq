#!/usr/bin/env python

import abc

class IPABC(object):
	addr_input = None

	def __init__(self, addr):
		super(object)
		self.addr_input = addr

	@abc.abstractmethod
	def valid(self, addr):
		return None
