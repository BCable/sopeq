#!/usr/bin/env python3

# Sopeq: outgoing.py
# License: GPLv2
# Author: Brad Cable

from service_instance import service_instance

class outgoing(service_instance):

	def __init__(self,
		controller, target, service, in_addr, out_addr, in_if=None, out_if=None
	):
		super().__init__(
			controller, target, service, out_addr, in_addr, out_if, in_if
		)

if __name__ == "__main__":
	assert outgoing(
		"accept", "ssh",
		"10.1.1.1/32", "10.0.0.0/8",
		"eth0", "eth1"
	).rules() == [
	("filter",
		"-A ssh_IN -d 10.0.0.0/8 -s 10.1.1.1/32 -i eth1 -o eth0 -j ACCEPT",
	),("filter",
		"-A ssh_OUT -s 10.0.0.0/8 -d 10.1.1.1/32 -o eth1 -i eth0 -j ACCEPT"
	)]

	assert outgoing(
		"accept", "ssh",
		None, "10.0.0.0/8",
		"eth0", "eth1"
	).rules() == [
	("filter",
		"-A ssh_IN -d 10.0.0.0/8 -i eth1 -o eth0 -j ACCEPT",
	),("filter",
		"-A ssh_OUT -s 10.0.0.0/8 -o eth1 -i eth0 -j ACCEPT"
	)]

	assert outgoing(
		"accept", "ssh",
		"10.1.1.1/32", None,
		"eth0", "eth1"
	).rules() == [
	("filter",
		"-A ssh_IN -s 10.1.1.1/32 -i eth1 -o eth0 -j ACCEPT",
	),("filter",
		"-A ssh_OUT -d 10.1.1.1/32 -o eth1 -i eth0 -j ACCEPT"
	)]

	assert outgoing(
		"accept", "ssh",
		"10.1.1.1/32", "10.0.0.0/8",
		None, "eth1"
	).rules() == [
	("filter",
		"-A ssh_IN -d 10.0.0.0/8 -s 10.1.1.1/32 -i eth1 -j ACCEPT",
	),("filter",
		"-A ssh_OUT -s 10.0.0.0/8 -d 10.1.1.1/32 -o eth1 -j ACCEPT"
	)]

	assert outgoing(
		"accept", "ssh",
		"10.1.1.1/32", "10.0.0.0/8",
		"eth0", None
	).rules() == [
	("filter",
		"-A ssh_IN -d 10.0.0.0/8 -s 10.1.1.1/32 -o eth0 -j ACCEPT",
	),("filter",
		"-A ssh_OUT -s 10.0.0.0/8 -d 10.1.1.1/32 -i eth0 -j ACCEPT"
	)]

	print("All asserts passed.")
