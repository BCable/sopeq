#!/usr/bin/env python

import sys

import pool

class ClassLoader(object):
	class_name = None
	method_name = None
	method_args = None
	command_args = None

	whitelist_modules = [
		"service", "filter", "nat", "pool", "policies",
	]
	blacklist_methods = []

	def __init__(self):
		pass

	def load_class(self, class_name):
		if class_name in self.whitelist_modules:
			self.class_name = class_name
			return True
		else:
			self.class_name = None
			return False

	def load_method(self, method_name):
		if (
			method_name[0:2] == "__" and method_name[-2:] == "__"
		) or method_name in self.blacklist_methods:
			self.method_name = None
			return False
		else:
			self.method_name = method_name
			return True

	def load_method_args(self, method_args):
		self.method_args = method_args

	def load_command_args(self, command_args):
		self.command_args = command_args

	def execute(self):
		if (
			self.class_name is None or self.method_name is None or
			self.method_args is None or self.command_args is None
		):
			return False

		method = sys.modules[self.class_name].__getattribute__(self.method_name)
		# TODO: check if method is of type
		command = method(*self.method_args)
		command.call(*self.command_args)
