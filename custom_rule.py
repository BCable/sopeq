#!/usr/bin/env python3

# Sopeq: custom_rule.py
# License: GPLv2
# Author: Brad Cable

from rule_generator import rule_generator

class custom_rule(rule_generator):
	_table = None
	_rule = None

	def __init__(self, table, rule):
		super().__init__()

		self._table = table
		self._rule = rule

	def rules(self):
		return [(self._table, self._rule)]
