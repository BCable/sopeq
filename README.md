Sopeq
=====

Firewall rules for egress/ingress filtering, NAT, and IP pools for easy IPTables management directed at hypervisors and routers.

MAJOR OVERHAUL UNDERWAY (2015-08-05)
------------------------------------

Sopeq is about to become a modular project I will maintain as production quality.  This hasn't really been production worthy in the past, but I've succesfully run large quantities of tests on production servers.

Check back in 3-4 weeks to simplify your life and improve your firewall security.  Teaser rule files:

Basic client:

    policies d:d:a
    templates.stateful

Basic Webserver with SSH:

    policies d:d:d
    templates.stateful

    # allow http traffic
    service.accept:http {all} {local}
    service.accept:https {all} {local}

Complex ingress/egress router:

    policies d:d:d
    templates.stateful

    # define NAT masquerade rules
    nat.masq:eth0

    # define a pool
    pool.define:subnet1 10.0.0.0/24
    pool.define:subnet2 10.1.0.0/24

    # join two pools in a new pool named 'local'
    pool.join:local {subnet1} {subnet2}

    # add allowed routed service from subnet1 to subnet2
    route.accept:http {subnet1} {subnet2}

    # allow outgoing traffic
    traffic.accept:http {local} {all}

Custom Services:

    # custom UDP service
    service.define:custom_name:in -p udp --dport 43121
    service.define:custom_name:out -p udp --sport 43121

Full Custom Rule Lines:

    # some random line I pulled from iptables manual about the 'recent' module
    filter.append:FORWARD:DROP -m recent --name badguy --rcheck --seconds 60
