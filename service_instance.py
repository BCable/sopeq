#!/usr/bin/env python3

# Sopeq: service_instance.py
# License: GPLv2
# Author: Brad Cable

from collections import OrderedDict

from iptables_helpers import *
from ipv4 import IPv4
from pool import pool
from rule_generator import rule_generator

# structured for incoming/traffic classes, outgoing generally will swap
# "in" variables for "out" variables

class service_instance(rule_generator):
	_controller = None
	_target = None
	service_name = None
	_log_prefix = None
	_rule_comment = None
	_in_addrs = None
	_out_addrs = None
	_in_if = None
	_out_if = None

	def __init__(self,
		controller, target, service_name, log_prefix, rule_comment,
		in_addrs, out_addrs, in_if=None, out_if=None
	):
		super().__init__()

		self._target = target.upper()

		self._controller = controller
		self.service_name = service_name
		self._log_prefix = log_prefix
		self._rule_comment = rule_comment
		self._in_addrs = in_addrs
		self._out_addrs = out_addrs
		self._in_if = in_if
		self._out_if = out_if

	def rules(self):
		in_pool = self._controller.get_pool(self._in_addrs)
		out_pool = self._controller.get_pool(self._out_addrs)

		if in_pool is not None:
			in_addrs = in_pool.get_all_ips()
		else:
			in_addrs = [self._in_addrs]

		if out_pool is not None:
			out_addrs = out_pool.get_all_ips()
		else:
			out_addrs = [self._out_addrs]

		ret_rules = []
		for in_addr in in_addrs:
			if not IPv4(in_addr).valid():
				raise Exception # TODO

			for out_addr in out_addrs:
				if not IPv4(out_addr).valid():
					raise Exception # TODO

				rules = self.rule(in_addr, out_addr)
				for rule in rules:
					if rule[1] != "out" or self._egress:
						ret_rules.append((rule[0], rule[2]))

		return ret_rules

	def rule(self, in_addr, out_addr):
		input_rule = "-A {}_IN".format(self.service_name)
		output_rule = "-A {}_OUT".format(self.service_name)

		if self._log_prefix is not None:
			input_rule_log = input_rule
			output_rule_log = output_rule

		input_flags = OrderedDict()
		output_flags = OrderedDict()

		if self._in_addrs is not None:
			input_flags["d"] = in_addr
			output_flags["s"] = in_addr

		if self._out_addrs is not None:
			input_flags["s"] = out_addr
			output_flags["d"] = out_addr

		if self._in_if is not None:
			input_flags["i"] = self._in_if
			output_flags["o"] = self._in_if

		if self._out_if is not None:
			input_flags["o"] = self._out_if
			output_flags["i"] = self._out_if

		# not necessary anymore since I"m changing how validate works
		### these are in order of what iptables outputs
		### (the first isn"t necessary since the others will move past it)
		###if "s" in input_flags:
		###	input_flags.move_to_end("s")
		###if "s" in output_flags:
		###	output_flags.move_to_end("s")
		##if "d" in input_flags:
		##	input_flags.move_to_end("d")
		##if "d" in output_flags:
		##	output_flags.move_to_end("d")
		##if "i" in output_flags:
		##	input_flags.move_to_end("i")
		##if "i" in output_flags:
		##	output_flags.move_to_end("i")
		##if "o" in input_flags:
		##	input_flags.move_to_end("o")
		##if "o" in output_flags:
		##	output_flags.move_to_end("o")

		input_flags["j"] = self._target
		output_flags["j"] = self._target

		for flag in input_flags:
			cur_append = " -{} {}".format(
				flag, input_flags[flag]
			)
			input_rule += cur_append
			if self._log_prefix is not None and flag != "j":
				input_rule_log += cur_append

		for flag in output_flags:
			cur_append = " -{} {}".format(
				flag, output_flags[flag]
			)
			output_rule += cur_append
			if self._log_prefix is not None and flag != "j":
				output_rule_log += cur_append

		if self._log_prefix is not None:
			log_append = ipt_construct_log(
				self._controller.log_level, self._log_prefix
			)
			input_rule_log += log_append
			output_rule_log += log_append

		if self._rule_comment is not None:
			comment_append = ipt_construct_comment(self._rule_comment)
			input_rule += comment_append
			output_rule += comment_append
			if self._log_prefix is not None:
				input_rule_log += comment_append
				output_rule_log += comment_append

		rule_list = [
			("filter", "in", input_rule), ("filter", "out", output_rule)
		]
		if self._log_prefix is not None:
			rule_list.extend([
				("filter", "in", input_rule_log),
				("filter", "out", output_rule_log)
			])

		return rule_list
