#!/usr/bin/env python3

# Sopeq: rule_generator.py
# License: GPLv2
# Author: Brad Cable

import abc

class rule_generator(object):
	_forwarding = False
	_egress = False

	def __init__(self):
		super().__init__()

	@abc.abstractmethod
	def chains(self):
		return None

	@abc.abstractmethod
	def rules(self):
		return None

	def generate(self, forwarding=False, egress=False):
		self._forwarding = forwarding
		self._egress = egress
