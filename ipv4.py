#!/usr/bin/env python

# Sopeq: ipv4.py
# License: GPLv2
# Author: Brad Cable

from ipabc import IPABC
import re

class IPv4(IPABC):

	def valid(self, addr=None):
		if addr is None:
			addr = self._addr_input

		re_octet = "([01]?[0-9]{1,2}|2([0-4][0-9]|5[0-5]))"
		re_mask = "([0-9]|[12][0-9]|3[0-2])"

		return re.match(
			"^({octet}\.){{3}}{octet}(/{mask})?$".format(
				octet = re_octet,
				mask = re_mask,
			),
			addr
		) is not None

# unit tests
if __name__ == "__main__":
	# valid IPv4
	assert(IPv4("10.0.0.0").valid() is True)
	assert(IPv4("10.0.0.0/8").valid() is True)
	assert(IPv4("10.0.0.0/16").valid() is True)
	assert(IPv4("010.000.001.001/0").valid() is True)

	# invalid IPv4
	assert(IPv4("256.0.0.0").valid() is False)
	assert(IPv4("10.265.0.0").valid() is False)
	assert(IPv4("10.0.300.0").valid() is False)
	assert(IPv4("10.0.0.1000").valid() is False)
	assert(IPv4("10.0.0.0/33").valid() is False)
	assert(IPv4("10.0.0.0/40").valid() is False)
	assert(IPv4("10.0.0.0/100").valid() is False)

	# tell user asserts were correct
	print("All asserts passed.")
