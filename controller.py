#!/usr/bin/env python3

# Sopeq: controller.py
# License: GPLv2
# Author: Brad Cable

from collections import OrderedDict
from configparser import ConfigParser
from copy import deepcopy

from os import getgid, getuid, listdir
from os.path import exists, isdir, join
from re import sub
from subprocess import Popen, PIPE
from sys import argv, exit, stdout
from time import sleep

from custom_rule import custom_rule
from incoming import incoming
from iptables_helpers import *
from outgoing import outgoing
from parser import line_parser
from pool import pool
from service import service
from traffic import traffic

SOPEQ_CONFIG = "/etc/sopeq/sopeq.conf"
SOPEQ_RULES_PATHS = [
	"/etc/sopeq/templates.conf",
	"/etc/sopeq/templates.conf.d",
	"/etc/sopeq/pools.conf",
	"/etc/sopeq/pools.conf.d",
	"/etc/sopeq/services.conf",
	"/etc/sopeq/services.conf.d",
	"/etc/sopeq/rules.conf",
	"/etc/sopeq/rules.conf.d"
]
IPTABLES_RESTORE_PATH = ["/usr/bin/env", "iptables-restore"]
IPTABLES_SAVE_PATH = ["/usr/bin/env", "iptables-save"]

class controller(object):
	_mode = None
	_forwarding = None
	_sleep_time = None
	_reject_with = None
	_default_logging = None
	log_level = None
	_egress = None
	_skip_standard_rules = None
	_include_related = None

	_policies = None

	_default_policies = None
	_pools = None
	_rules = None
	_services = None
	_services_used = None
	_stored_tables = None

	def __init__(self):
		pass

	def reset_defaults(self):
		self._mode = "stealth"
		self._forwarding = False
		self._sleep_time = 60
		self._reject_with = "icmp-net-prohibited"
		self._default_logging = True
		self.log_level = "warning"
		self._egress = False
		self._skip_standard_rules = False
		self._include_related = False

		policies = OrderedDict()

		# set default iptables recognized policies
		policies["security"] = OrderedDict()
		policies["security"]["INPUT"] = "ACCEPT"
		policies["security"]["FORWARD"] = "ACCEPT"
		policies["security"]["OUTPUT"] = "ACCEPT"

		policies["mangle"] = OrderedDict()
		policies["mangle"]["PREROUTING"] = "ACCEPT"
		policies["mangle"]["INPUT"] = "ACCEPT"
		policies["mangle"]["FORWARD"] = "ACCEPT"
		policies["mangle"]["OUTPUT"] = "ACCEPT"
		policies["mangle"]["POSTROUTING"] = "ACCEPT"

		policies["raw"] = OrderedDict()
		policies["raw"]["PREROUTING"] = "ACCEPT"
		policies["raw"]["OUTPUT"] = "ACCEPT"

		policies["nat"] = OrderedDict()
		policies["nat"]["PREROUTING"] = "ACCEPT"
		policies["nat"]["INPUT"] = "ACCEPT"
		policies["nat"]["OUTPUT"] = "ACCEPT"
		policies["nat"]["POSTROUTING"] = "ACCEPT"

		policies["filter"] = OrderedDict()
		policies["filter"]["INPUT"] = "ACCEPT"
		policies["filter"]["FORWARD"] = "ACCEPT"
		policies["filter"]["OUTPUT"] = "ACCEPT"

		# deepcopy since it will be compared against the set policies later
		self._default_policies = deepcopy(policies)
		self._policies = policies

	def load_config(self, filename):
		if not exists(filename):
			return

		cp = ConfigParser()
		cp.read(filename)

		sections = cp.sections()
		if "global" in sections:
			sections.remove("global")
			options = cp.options("global")

			if "mode" in options:
				options.remove("mode")
				mode = cp.get("global", "mode")
				if self._mode not in ("stealth", "standard"):
					raise Exception

				self._mode = mode

			if "forwarding" in options:
				options.remove("forwarding")

				try:
					forwarding = cp.getboolean("global", "forwarding")
				except ValueError:
					raise Exception # TODO

				self._forwarding = forwarding

			if "sleep_time" in options:
				options.remove("sleep_time")

				try:
					sleep_time = cp.getint("global", "sleep_time")
				except ValueError:
					raise Exception # TODO

				if sleep_time < 1:
					raise Exception # TODO

				self._sleep_time = sleep_time

			if "reject_with" in options:
				options.remove("reject_with")
				reject_with = cp.get("global", "reject_with")
				if self._reject_with not in (
					# these are taken from the iptables-extensions(8) man page
					# IPv4
					"icmp-net-unreachable", "icmp-host-unreachable",
					"icmp-port-unreachable", "icmp-proto-unreachable",
					"icmp-net-prohibited", "icmp-host-prohibited",
					"icmp-admin-prohibited",
					# IPv6
					"icmp6-no-route", "no-route", "icmp6-adm-prohibited",
					"adm-prohibited", "icmp6-addr-unreachable",
					"addr-unreach", "icmp6-port-unreachable",
				):
					raise Exception

				self._reject_with = reject_with

			if "default_logging" in options:
				options.remove("default_logging")

				try:
					default_logging = cp.getboolean("global", "default_logging")
				except ValueError:
					raise Exception # TODO

				self._default_logging = default_logging

			if "log_level" in options:
				options.remove("log_level")

				try:
					log_level = cp.get("global", "log_level")
				except ValueError:
					raise Exception # TODO

				if log_level not in (
					"alert", "crit", "error", "warning",
					"notice", "info", "debug"
				):
					raise Exception # TODO

				self.log_level = log_level

			if "egress" in options:
				options.remove("egress")

				try:
					egress = cp.getboolean("global", "egress")
				except ValueError:
					raise Exception # TODO

				self._egress = egress

			if "skip_standard_rules" in options:
				options.remove("skip_standard_rules")

				try:
					skip_standard_rules = \
						cp.getboolean("global", "skip_standard_rules")
				except ValueError:
					raise Exception # TODO

				self._skip_standard_rules = skip_standard_rules

			if "include_related" in options:
				options.remove("include_related")

				try:
					include_related = \
						cp.getboolean("global", "include_related")
				except ValueError:
					raise Exception # TODO

				self._include_related = include_related

			if len(options) > 0:
				raise Exception # TODO

		if "policies" in sections:
			sections.remove("policies")
			options = cp.options("policies")

			for option in options:
				option_split = option.split("_")

				if len(option_split) != 2:
					raise Exception # TODO

				table, chain = option_split
				chain = chain.upper()

				if table not in self._policies:
					raise Exception # TODO

				if chain not in self._policies[table]:
					raise Exception # TODO

				new_policy = cp.get("policies", option)

				if new_policy not in ("ACCEPT", "DROP", "REJECT"):
					raise Exception # TODO

				self._policies[table][chain] = new_policy

		if len(sections) > 0:
			raise Exception # TODO

	@staticmethod
	def get_obj_index(obj, name):
		for i in range(0, len(obj)):
			if obj[i].name == name:
				return i
		return None

	@staticmethod
	def get_obj(obj, obj_name):
		idx = controller.get_obj_index(obj, obj_name)
		if idx is None:
			return None
		else:
			return obj[idx]

	def get_pool_index(self, pool_name):
		return controller.get_obj_index(self._pools, pool_name)

	def get_pool(self, pool_name):
		return controller.get_obj(self._pools, pool_name)

	def get_service_index(self, service_name):
		return controller.get_obj_index(self._services, service_name)

	def get_service(self, service_name):
		return controller.get_obj(self._services, service_name)

	def initial_rules(self):
		self._services = []
		self._services_used = []
		self._pools = []

		if self._skip_standard_rules:
			self._rules = []
		else:
			self._rules = [custom_rule("filter",
				"-A INPUT -m state --state INVALID -j DROP"
			)]
			if self._include_related:
				self._rules.append(custom_rule("filter",
					"-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT"
				))
			else:
				self._rules.append(custom_rule("filter",
					"-A INPUT -m state --state ESTABLISHED -j ACCEPT"
				))

			self._rules.append(custom_rule("filter",
				"-A FORWARD -m state --state INVALID -j DROP"
			))
			if self._forwarding:
				if self._include_related:
					self._rules.append(custom_rule("filter",
						"-A FORWARD -m state --state RELATED,ESTABLISHED " +
						"-j ACCEPT"
					))
				else:
					self._rules.append(custom_rule("filter",
						"-A FORWARD -m state --state ESTABLISHED -j ACCEPT"
					))

			self._rules.append(custom_rule("filter",
				"-A OUTPUT -m state --state INVALID -j DROP"
			))
			if self._egress:
				if self._include_related:
					self._rules.append(custom_rule("filter",
						"-A OUTPUT -m state --state RELATED,ESTABLISHED " +
						"-j ACCEPT"
					))
				else:
					self._rules.append(custom_rule("filter",
						"-A OUTPUT -m state --state ESTABLISHED -j ACCEPT"
					))

	def load_rules(self, filename):
		fd = open(filename, "rb")
		for line in fd.readlines():
			ret_obj = line_parser(self, line).parse()
			if type(ret_obj) == pool:
				idx = self.get_pool_index(ret_obj.name)
				if idx is None:
					self._pools.append(ret_obj)
				else:
					self._pools[idx] = ret_obj

			elif type(ret_obj) == service:
				idx = self.get_service_index(ret_obj.name)
				if idx is None:
					self._services.append(ret_obj)
				else:
					raise Exception # TODO: service already defined

			elif ret_obj is not None:
				if type(ret_obj) in (incoming, traffic):
					service_name = "{}:in".format(ret_obj.service_name)
					idx = self.get_service_index(service_name)
					if idx is None:
						raise Exception # TODO: service not defined
					else:
						self._services_used.append(service_name)

				if type(ret_obj) in (outgoing, traffic):
					service_name = "{}:out".format(ret_obj.service_name)
					idx = self.get_service_index(service_name)
					if idx is None:
						raise Exception # TODO: service not defined
					else:
						self._services_used.append(service_name)

				self._rules.append(ret_obj)

		fd.close()

	def iptables_output(self, table):
		rules = []
		chains = []
		ret = ""

		# set default sopeq policies based on mode
		policies = deepcopy(self._policies)
		if policies["filter"]["INPUT"] == "ACCEPT":
			if self._mode == "stealth":
				policies["filter"]["INPUT"] = "DROP"
			else:
				policies["filter"]["INPUT"] = "REJECT"
		if policies["filter"]["FORWARD"] == "ACCEPT":
			if self._mode == "stealth":
				policies["filter"]["FORWARD"] = "DROP"
			else:
				policies["filter"]["FORWARD"] = "REJECT"

		if self._egress and policies["filter"]["OUTPUT"] == "ACCEPT":
			if self._mode == "stealth":
				policies["filter"]["OUTPUT"] = "DROP"
			else:
				policies["filter"]["OUTPUT"] = "REJECT"

		# preprocessing of rulesets
		for ruleset in self._rules + self._services:
			if (
				type(ruleset) == service and
				not ruleset.name in self._services_used
			):
				continue

			ruleset.generate(self._forwarding, self._egress)
			def_chains = ruleset.chains()
			def_rules = ruleset.rules()

			if def_chains is not None:
				for chain in def_chains:
					if chain[0] not in policies:
						raise Exception # TODO

					if chain[0] == table:
						chains.append(chain[1])

			if def_rules is not None:
				for rule in def_rules:
					if rule[0] not in policies:
						raise Exception # TODO

					if rule[0] == table:
						rules.append(rule[1])

		# write table header
		ret = "*{}\n".format(table)

		# write policies
		for chain in policies[table]:
			policy = policies[table][chain]
			if policy == "REJECT":
				policy = "DROP"

			ret += ":{} {}\n".format(chain, policy)

		# write new chains if they exist
		if len(chains) != 0:
			chains.sort()
			for chain in chains:
				ret += ":{} -\n".format(chain)

		# write rules if they exist
		if len(rules) != 0:
			for rule in rules:
				ret += "{}\n".format(rule)

		# write logging and reject rules if necessary
		for chain in self._policies[table]:
			if self._default_logging:
				if policies[table][chain] != "ACCEPT":
					ret += "-A {}{}\n".format(chain, ipt_construct_log(
						self.log_level, "{}_{}".format(table, chain)
					))

			if policies[table][chain] == "REJECT":
				ret += "-A {} -j REJECT --reject-with {}\n".format(
					chain, self._reject_with
				)

		return ret + "COMMIT\n"

	def enforce(self, fd=stdout, write=True):
		input_rules = ""
		for table in self._policies:
			input_rules += self.iptables_output(table)

		# write rules to iptables tables/chains
		if write:
			proc = Popen(
				IPTABLES_RESTORE_PATH, stdin=PIPE, stdout=PIPE, stderr=PIPE
			)
			ret_stdout, ret_stderr = proc.communicate(input_rules.encode())

			if proc.returncode != 0:
				raise Exception # TODO

			self.validate(True)

		# debug mode, so print to screen
		else:
			print(input_rules)

	def validate(self, store=False):
		output_rules = b""

		# loop tables to output current ruleset in iptables
		for table in self._policies:
			proc = Popen(
				IPTABLES_SAVE_PATH + ["-t", table], stdout=PIPE, stderr=PIPE
			)
			ret_stdout, ret_stderr = proc.communicate()

			if proc.returncode != 0:
				raise Exception # TODO

			output_rules += ret_stdout

		# store a copy instead of validating (first run or reload)
		if store:
			self._stored_tables = output_rules
			return True

		# compare against existing
		else:
			return ipt_clean(output_rules) == ipt_clean(self._stored_tables)

if __name__ == "__main__":
	if getuid() != 0 or getgid() != 0:
		print("ERROR: Sopeq must be run as root.") # TODO
		exit(1)

	control = controller()
	control.reset_defaults()
	control.initial_rules()
	control.load_config(SOPEQ_CONFIG)

	for rules_path in SOPEQ_RULES_PATHS:
		if exists(rules_path):
			if not isdir(rules_path):
				control.load_rules(rules_path)
			else:
				for conf_file in listdir(rules_path):
					control.load_rules(join(rules_path, conf_file))

	if len(argv) > 1 and argv[1] == "-d":
		control.enforce(write=False)
	else:
		control.enforce()
		#sleep(10)
		print(control.validate())
